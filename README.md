VRChatのワールド制作をgitで行うワークフローを模索するためのプロジェクトです。
プロジェクトはMITライセンスでオープンソースとするため、**MITライセンスに同意できない場合はコミットしないでください。**

再配布不可能なアセットはignoreされています。cloneした後、実行前に以下のアセットを入れ直して作業してください。

# 実行に必要なアセット

* VRC SDK https://www.vrchat.net/home/download
* Free HDR Sky https://assetstore.unity.com/packages/2d/textures-materials/sky/free-hdr-sky-61217
* ProBuilder https://assetstore.unity.com/packages/tools/modeling/probuilder-111418
* Standard Assets https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-32351
* ToyboxV2 https://www.dropbox.com/s/k0hui254bx4ozzv/VRCPrefabToyboxV2.1.unitypackage

# MIT Credits

Assets/ExMenu, chiugame, https://chiugame.booth.pm/items/1510744
Assets/iwashibox, IWASHI-184.COM, https://gitlab.com/iwashi_farm/iwashibox/releases
