﻿
// Created by Kurotori4423. 
// Blackhole Model based on Johan Svensson https://medium.com/dotcrossdot.

Shader "KurotoriShader/BlackHoleRaymarching"
{
	Properties
	{
		_MaxStep("Max Step", Int) = 200
		_StepSize("Step Size", Float) = 0.004
		_BlackHoleColor("Black hole color", Color) = (0,0,0,1)
		_SchwarzschildRadius("schwarzschildRadius", Float) = 0.5
		_SpaceDistortion("Space distortion", Float) = 4.069
		_BackDistortion("Back ground distortion", Float) = 50
		_AccretionDiskColor("Accretion disk color", Color) = (1,1,1,1)
		_AccretionDiskThickness("Accretion disk thickness", Float) = 1
		_AccretionDiskRadius("Accretion disk radius", Float) = 3.5
		_AccretionDiskSpeed("Accretion disk speed", Float) = 1.5
		_AccretionDiskTwist("Accretion disk twist", Float) = 3
		_Noise("Accretion disk noise", 2D) = "" {}
	}

	SubShader
	{
		// No culling or depth
		Tags { "RenderType" = "Transparent"
			"Queue" = "Transparent" }

		GrabPass{}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			// Set from script.
			uniform float4x4 _FrustumCornersES;
			uniform float4x4 _CameraInvViewMatrix;
			uniform float3 _CameraWS;
			sampler2D _GrabTexture;
			uniform sampler2D _CameraDepthTexture;
			float2 _CameraDepthTexture_ST;
			float4 _CameraDepthTexture_TexelSize;
			uniform int _MaxStep;
			uniform float _StepSize;
			uniform float _BackDistortion;

			// Set from material.
			uniform sampler2D _Noise;
			float _SpaceDistortion;
			float _SchwarzschildRadius;
			half4 _AccretionDiskColor;
			half4 _BlackHoleColor;
			float _AccretionDiskThickness;
			float _AccretionDiskRadius;
			float _AccretionDiskSpeed;
			float _AccretionDiskTwist;

			struct appdata
			{
				// The z value here contains the index of _FrustumCornersES to use
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 oPos : TEXCOORD1;
				float4 grabPos : TEXCOORD2;
				float3 viewDir : TEXCOORD3;
				float4 scrPos : TEXCOORD4;
				float3 normal : NORMAL;

			};

			// 球体の距離関数
			float sdSphere(float3 p, float s)
			{
				return length(p) - s;
			}

			// 角が取れたシリンダーの距離関数
			float sdRoundedCylinder(float3 p, float ra, float rb, float h)
			{
				float2 d = float2(length(p.xz) - 2.0 * ra + rb, abs(p.y) - h);
				return min(max(d.x, d.y), 0.0) + length(max(d, 0.0)) - rb;
			}

			// スムースな減算
			float opSmoothSubtraction(float d1, float d2, float k) {
				float h = clamp(0.5 - 0.5 * (d2 + d1) / k, 0.0, 1.0);
				return lerp(d2, -d1, h) + k * h * (1.0 - h);
			}

			// A SDF combination creating something that looks like an accretion disk.
			// Made up of a flattened rounded cylinder from which we subtract a sphere.
			// 降着円盤SDF
			// SDF : 距離関数
			float accretionDiskSDF(float3 p) {
				float p1 = sdRoundedCylinder(p, _AccretionDiskRadius, 0.01, 0.001);
				float p2 = sdSphere(p, _AccretionDiskRadius);
				return opSmoothSubtraction(p2, p1, 0.1);
			}

			// An (very rough!!) approximation of how light is bent given the distance to a black hole. 
			// ブラックホールまでの距離が与えられた場合の、光の曲がり方の（非常に大まかな!!）近似。
			float GetSpaceDistortionLerpValue(float schwarzschildRadius, float distanceToSingularity, float spaceDistortion) {
				return pow(schwarzschildRadius, spaceDistortion) / pow(distanceToSingularity, spaceDistortion);
			}

			float2 AlignWithGrabTexel(float2 uv)
			{
				return (floor(uv * _CameraDepthTexture_TexelSize.zw) + 0.5) * abs(_CameraDepthTexture_TexelSize.xy);
			}

			fixed4 raymarch(float3 ro, float3 rd, float4 grabPos, float rim, float4 ase_screenPos) {
				fixed4 ret = _AccretionDiskColor;
				ret.a = 0;

				float3 previousPos = ro;
				float epsilon = 0.01;
				float thickness = 0;

				float3 previousRayDir = rd;
				float3 blackHolePosition = float3(0, 0, 0);
				float distanceToSingularity = 99999999;
				float blackHoleInfluence = 0;
				half4 lightAccumulation = half4(0, 0, 0, 1);
				half noiseScale = 1;

				for (int i = 0; i < _MaxStep; ++i) {
					// Get two vectors. One pointing in previous direction and one pointing to the singularity. 
					// 2つのベクトルを取得します。 1つは前の方向を指し、もう1つは特異点を指します。
					float3 unaffectedDir = normalize(previousRayDir) * _StepSize;
					float3 maxAffectedDir = normalize(blackHolePosition - previousPos) * _StepSize;
					distanceToSingularity = distance(blackHolePosition, previousPos);

					// Calculate how to interpolate between the two previously calculated vectors.
					// 以前に計算された2つのベクトル間を補間する方法を計算します。
					float lerpValue = GetSpaceDistortionLerpValue(_SchwarzschildRadius, distanceToSingularity, _SpaceDistortion);
					float3 newRayDir = normalize(lerp(unaffectedDir, maxAffectedDir, lerpValue)) * _StepSize;

					// Move the lightray along and calculate the sdf result
					// lightrayを移動して、sdfの結果を計算します
					float3 newPos = previousPos + newRayDir;
					float sdfResult = accretionDiskSDF(newPos);

					// Inside the acceration disk. Sample light.
					// 膠着円盤の内側。光をサンプル
					if (sdfResult < epsilon) {
						// Rotate the texture sampling to fake motion.

						float u = cos(frac(_Time.z * _AccretionDiskSpeed) * 10 * (1.0f - distanceToSingularity) * _AccretionDiskTwist);
						float v = sin(frac(_Time.z * _AccretionDiskSpeed) * 10 * (1.0f - distanceToSingularity) * _AccretionDiskTwist);
 
						float u2 = cos(frac(_Time.z * _AccretionDiskSpeed + 0.5f) * 10 * (1.0f - distanceToSingularity) * _AccretionDiskTwist);
						float v2 = sin(frac(_Time.z * _AccretionDiskSpeed + 0.5f) * 10 * (1.0f - distanceToSingularity) * _AccretionDiskTwist);

						float2x2 rot = float2x2(u, -v, v, u);
						float2 uv = mul(rot, newPos.xz * noiseScale) + 0.5f;

						float2x2 rot2 = float2x2(u2, -v2, v2, u2);
						float2 uv2 = mul(rot2, newPos.xz * noiseScale) + 0.5f;


						// Get thickness from the noise texture.
						float noise = pow(tex2D(_Noise, uv).r, 2);
						float noise2 = pow(tex2D(_Noise, uv2).r, 2);
						thickness = lerp(noise2, noise, 1- abs(1 -2 * frac(_Time.z * _AccretionDiskSpeed))) * _AccretionDiskThickness;

						// Add to the rays light accumulation.
						lightAccumulation += _AccretionDiskColor * thickness;
					}

					// Calculate black hole influence on the final color.
					blackHoleInfluence = step(distanceToSingularity, _SchwarzschildRadius);
					previousPos = newPos;
					previousRayDir = newRayDir;
				}

				float3 worldDir = mul(unity_ObjectToWorld, float4(previousRayDir, 0.0f));

				float3 skyColorIBL = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, worldDir, 0);

				float3 b = dot(previousRayDir, rd)*rd;

				float2 distotion = mul(unity_ObjectToWorld, float4((previousRayDir - b), 0.0f)).xy * rim * _BackDistortion;

				ase_screenPos.xy = ase_screenPos.xy + distotion;

				float depth = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(ase_screenPos))));
				float surfDepth =  ase_screenPos.z;
				float depthDiff = saturate(depth - surfDepth);
				float2 grabUV = AlignWithGrabTexel((grabPos.xy + distotion * depthDiff) / grabPos.w);
				float3 skyColor = tex2D(_GrabTexture, grabUV).rgb;

				// Sample let background be either skybox or the black hole color.
				half4 backGround = lerp(float4(skyColor.rgb, 0), _BlackHoleColor, blackHoleInfluence);

				// Return background and light.
				return backGround + lightAccumulation ;
			}

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.oPos = v.vertex.xyz;
				o.grabPos = ComputeGrabScreenPos(o.vertex);
				o.normal = v.normal;
				o.scrPos = ComputeScreenPos(o.vertex);
				o.viewDir = normalize(ObjSpaceViewDir(v.vertex));
				COMPUTE_EYEDEPTH(o.scrPos.z);

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				//カメラの座標（オブジェクト座標系）
				float3 cameraOPos = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
				// ray direction
				float3 rd = normalize(i.oPos - cameraOPos);
				// ray origin (camera position)
				float3 ro = i.oPos;

				half2 uv = half2(i.grabPos.x / i.grabPos.w, i.grabPos.y / i.grabPos.w);

				float4 depthUV = i.scrPos;
				depthUV.xy = i.scrPos.xy;

				float4 ase_screenPos = float4(i.scrPos.xyz, i.scrPos.w + 0.00000000001);
				float rim = pow(dot(i.viewDir , i.normal),4);

				fixed4 col = raymarch(ro, rd, i.grabPos, rim, ase_screenPos);

				return col;
			}
			ENDCG
		}
	}
}